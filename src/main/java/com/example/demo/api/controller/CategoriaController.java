package com.example.demo.api.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.api.model.Categoria;
import com.example.demo.api.repository.CategoriaRepository;

@RestController
@Controller
@CrossOrigin("${allowed-cross-origin}")
@RequestMapping("/categorias")
public class CategoriaController {

	@Autowired
	private CategoriaRepository categoriaRepository;
	
	@GetMapping
	public List<Categoria> index() {
		return categoriaRepository.findAll();
	}
	
	@PostMapping
	public Categoria create(@RequestBody @Valid Categoria categoria) {
		return categoriaRepository.save(categoria);
	}
	
	@PutMapping("/{codigo}")
	public ResponseEntity<Categoria> update(@PathVariable Long codigo, @Valid @RequestBody Categoria categoria) {
		Categoria categoriaSalva = categoriaRepository.save(categoria);
		return categoriaSalva != null ? ResponseEntity.ok(categoriaSalva) : ResponseEntity.notFound().build();
		
	}
	
	@DeleteMapping("/{codigo}")
	public void delete(@PathVariable Long codigo) {
		categoriaRepository.delete(codigo);
	}
	
	@GetMapping("/{codigo}")	
	public Categoria findById(@PathVariable Long codigo) {
		return categoriaRepository.findOne(codigo);
	}
	

	
}
