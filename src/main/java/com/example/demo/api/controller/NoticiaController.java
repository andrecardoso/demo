package com.example.demo.api.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.api.model.Noticia;
import com.example.demo.api.repository.NoticiaRepository;

@RestController
@Controller
@CrossOrigin("${allowed-cross-origin}")
@RequestMapping("/noticias")
public class NoticiaController {

	@Autowired
	private NoticiaRepository noticiaRepository;
	
	@GetMapping
	public List<Noticia> index() {
		return noticiaRepository.findAll();
	}
	
	@PostMapping
	public Noticia create(@RequestBody @Valid Noticia noticia) {
		return noticiaRepository.save(noticia);
	}
	
	@PutMapping("/{codigo}")
	public ResponseEntity<Noticia> update(@PathVariable Long codigo, @Valid @RequestBody Noticia noticia) {
		Noticia noticiaSalva = noticiaRepository.save(noticia);
		return noticiaSalva != null ? ResponseEntity.ok(noticiaSalva) : ResponseEntity.notFound().build();		
	}
	
	@DeleteMapping("/{codigo}")
	public void delete(@PathVariable Long codigo) {
		noticiaRepository.delete(codigo);
	}
	
	@GetMapping("/{codigo}")	
	public Noticia findById(@PathVariable Long codigo) {
		return noticiaRepository.findOne(codigo);
	}
	

	
}
