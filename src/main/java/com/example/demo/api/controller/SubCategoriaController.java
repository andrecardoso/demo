package com.example.demo.api.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.api.model.Subcategoria;
import com.example.demo.api.repository.SubcategoriaRepository;

@RestController
@Controller
@CrossOrigin("${allowed-cross-origin}")
@RequestMapping("/subcategorias")
public class SubCategoriaController {

	@Autowired
	private SubcategoriaRepository subCategoriaRepository;
	
	@GetMapping
	public List<Subcategoria> index() {
		return subCategoriaRepository.findAll();
	}
	
	@PostMapping
	public Subcategoria create(@RequestBody @Valid Subcategoria subcategoria) {
		return subCategoriaRepository.save(subcategoria);
	}
	
	@PutMapping("/{codigo}")
	public Subcategoria update(@PathVariable Long codigo) {
		Subcategoria subcategoriaSalva = subCategoriaRepository.findOne(codigo);
		return subCategoriaRepository.save(subcategoriaSalva);
		
	}
	
	@DeleteMapping("/{codigo}")
	public void delete(@PathVariable Long codigo) {
		subCategoriaRepository.delete(codigo);
	}
	
	@GetMapping("/{codigo}")	
	public Subcategoria findById(@PathVariable Long codigo) {
		return subCategoriaRepository.findOne(codigo);
	}
	

	
}
