package com.example.demo.api.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.api.model.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

	public abstract Optional<Usuario> findByCodigo(Long codigo);
	
	public abstract Optional<Usuario> findByEmail(String email);
	
}
