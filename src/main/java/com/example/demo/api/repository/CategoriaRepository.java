package com.example.demo.api.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.api.model.Categoria;

public interface CategoriaRepository extends JpaRepository<Categoria, Long> {

	public abstract Optional<Categoria> findByCodigo(Long codigo);
}
