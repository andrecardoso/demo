package com.example.demo.api.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.api.model.Subcategoria;

public interface SubcategoriaRepository extends JpaRepository<Subcategoria, Long> {

	public abstract Optional<Subcategoria> findByCodigo(Long codigo);
	
}
