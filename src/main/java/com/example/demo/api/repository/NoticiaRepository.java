package com.example.demo.api.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.api.model.Noticia;

public interface NoticiaRepository extends JpaRepository<Noticia, Long> {

	public abstract Optional<Noticia> findByCodigo(Long codigo);
	
}
